from django.urls import reverse_lazy
from django.shortcuts import redirect

from django.contrib.auth import login
from django.contrib.auth.forms import UserCreationForm

from django.utils.decorators import method_decorator
from django.views.decorators.debug import sensitive_post_parameters

from django.views.generic import CreateView

class RegisterView(CreateView):
    template_name = 'registration/register.html'
    form_class = UserCreationForm
    success_url = reverse_lazy('quotes:index')

    @method_decorator(sensitive_post_parameters('password1', 'password2'))
    def dispatch(self, request, *args, **kwargs):
        """Disallow registered users from registering another account"""
        if request.user.is_authenticated:
            # TODO: Make this redirect to their user profile
            return redirect('/')

        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        """Log in user after registering"""
        valid_form = super().form_valid(form)
        login(self.request, form.instance)

        return valid_form
