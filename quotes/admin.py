from django.contrib import admin, messages
from django.utils.translation import ngettext

from .models import Quote

@admin.register(Quote)
class QuoteAdmin(admin.ModelAdmin):
    autocomplete_fields = ['submitter', 'approver']
    fields = ('submitter', 'approver', 'content', ('pub_date', 'is_published'))
    list_filter = ('approver', 'is_published')
    list_display = ('__str__', 'submitter', 'approver', 'content', 'is_published')

    actions = ['approve_quote']

    def approve_quote(self, request, queryset):
        updated = queryset.update(
            approver=request.user,
            is_published=True
        )
        self.message_user(request, ngettext(
            '%d quote was published.',
            '%d quotes were published.',
            updated,
        ) % updated, messages.SUCCESS)
    approve_quote.short_description = 'Approve selected quotes'

