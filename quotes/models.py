from django.db import models
from django.utils import timezone
from django.urls import reverse
from django.conf import settings
from django.utils.functional import cached_property

class Quote(models.Model):
    submitter = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='submitter_user',
        blank=True,
        null=True
    )
    approver = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        limit_choices_to={'is_staff': True},
        related_name='approver_user',
        blank=True,
        null=True
    )
    pub_date = models.DateTimeField(
        'Occurence date',
        default=timezone.now,
        help_text='The date and time that this quote or conversation occured.'
    )
    content = models.TextField(max_length=1000)
    is_published = models.BooleanField(
        'Published status',
        default=False,
        help_text='Quote is approved and published on the site.'
    )

    def __str__(self):
        return f"#{self.pk}"

    def get_absolute_url(self):
        return reverse('quotes:detail', kwargs={'pk': self.pk})

    @cached_property
    def lines(self):
        return self.content.split('\n')

    class Meta:
        ordering = ['-pub_date']
