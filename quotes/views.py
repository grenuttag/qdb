from django.urls import reverse
from django.core.exceptions import PermissionDenied
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic import ListView, DetailView, CreateView

from .models import Quote


class QuoteList(ListView):
    paginate_by = 10
    context_object_name = 'quotes'

    def get_queryset(self):
        if self.request.user.is_staff:
            return Quote.objects.all()
        else:
            return Quote.objects.filter(is_published=True)


class QuoteDetail(DetailView):
    model = Quote

    def get_object(self, queryset=None):
        quote = super().get_object()

        if quote.is_published or quote.submitter is self.request.user or self.request.user.is_staff:
            return quote
        else:
            raise PermissionDenied('User lacks permission to view requested quote')


class SubmitQuote(SuccessMessageMixin, CreateView):
    model = Quote
    fields = ['content']

    def form_valid(self, form):
        if self.request.user.is_authenticated:
            form.instance.submitter = self.request.user

        # Automatically publish submissions made by staff
        if self.request.user.is_staff:
            form.instance.approver = self.request.user
            form.instance.is_published = True

        return super().form_valid(form)

    def get_success_url(self):
        if self.request.user.is_authenticated:
            return self.object.get_absolute_url()
        else:
            return reverse('quotes:index')

    def get_success_message(self, cleaned_data):
        if self.request.user.is_staff:
            return 'Quote successfully added.'
        else:
            return '''Your quote was successfully submitted. It is
    currently under moderator review.'''
